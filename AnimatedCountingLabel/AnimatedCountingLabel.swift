//
//  AnimatedLabel.swift
//  AnimatedCounter
//
//  Created by Himanshu Goyal on 24/09/20.
//

import UIKit

class AnimatedCountingLabel: UILabel{
    
    enum OutputType{
        case integer
        case double
    }
    
    fileprivate let startValue: Double
    
    fileprivate var displayLink: CADisplayLink?
    
    fileprivate let endValue: Double
    
    fileprivate let animationDuration: Double
    
    fileprivate let animationStartDate = Date()
    
    fileprivate let outputType : OutputType?
    
    init(startValue: Double, endValue: Double, animationDuration: Double? = 1.5, outputType: OutputType? = .integer, textAlignment: NSTextAlignment? = .center, font: UIFont? = UIFont.systemFont(ofSize: 18)) {
        
        self.startValue = startValue
        
        self.endValue = endValue
        
        self.animationDuration = animationDuration ?? 1.5
        
        self.outputType = outputType
        
        super.init(frame: .zero)
        
        if self.outputType == .integer{
            self.text = "\(Int(startValue))"
        }else{
            self.text = "\(startValue)"
        }
        
        //Set Text Alignment of a label
        self.textAlignment = textAlignment ?? .center
        
        //Set Text Font
        self.font = font
        
        // Create CADisplayLink
        let displayLink = CADisplayLink(target: self, selector: #selector(handleUpdate))
        displayLink.add(to: .main, forMode: .default)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleUpdate(){
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        if elapsedTime > animationDuration{
            if self.outputType == .integer{
                self.text = "\(Int(endValue))"
            }else{
                self.text = "\(endValue)"
            }
            self.stopDisplayLink()
        }else{
            let percentage = elapsedTime / animationDuration
            let value = startValue + percentage * (endValue - startValue)
            if self.outputType == .integer{
                self.text = "\(Int(value))"
            }else{
                self.text = "\(value)"
            }
            self.stopDisplayLink()
        }
    }
    
    func stopDisplayLink() {
        displayLink?.invalidate()
        displayLink = nil
    }
}
