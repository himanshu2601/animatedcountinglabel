//
//  ViewController.swift
//  AnimatedCountingLabel
//
//  Created by Himanshu Goyal on 24/09/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.addCADisplayLink()
    }
    
    let countingLabel = AnimatedCountingLabel(startValue: 0, endValue: 1000, animationDuration: 3.0, outputType: .integer)
    
    private func addCADisplayLink(){
        countingLabel.frame = view.frame
        self.view.addSubview(countingLabel)
    }

}

